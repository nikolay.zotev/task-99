import { Button, Card, Form, Input } from "./form";

export default function LoginForm() {
  return (
    <Card>
      <h1>Login Form</h1>
      <Form>
        <Input type="email" placeholder="Email" />
        <Input type="password" placeholder="Password" />
        <Button>Login</Button>
      </Form>
    </Card>
  );
}
